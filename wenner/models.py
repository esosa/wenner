from django.db import models
from datetime import date

class Estudio(models.Model):
    fecha = models.TextField(default='')
    nombre = models.TextField(default='')
    ubicacion = models.TextField(default='')
    humedad = models.TextField(default='')
    numero_mediciones = models.IntegerField(default=-1)
    cantidad_estratos = models.IntegerField(default=-1)
    grafico_r_funcion_a = models.ImageField(upload_to='wenner/graficos', default=None)
    grafico_modelo = models.ImageField(upload_to='wenner/graficos', default=None)

class Medicion(models.Model):
    distancia = models.FloatField(default=0)
    resistividad = models.FloatField(default=0)
    estudio = models.ForeignKey(Estudio, default=None)

class Estrato(models.Model):
    espesor = models.FloatField(default=-1)
    resistividad = models.FloatField(default=-1)
    estudio = models.ForeignKey(Estudio, default=None)
