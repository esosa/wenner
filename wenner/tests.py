from django.test import TestCase
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.core.files.base import ContentFile

from wenner.views import home_page
from wenner.models import Medicion, Estudio, Estrato

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from io import BytesIO

class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_redirige_despues_de_POST(self):
        response = self.client.post(
            '/wenner/crea_estudio',
            data = {'num_mediciones': '1'},
        )
        estudio = Estudio.objects.first()
        self.assertRedirects(response, "/wenner/%d/introduce_estudio" % (estudio.id))

class IntroduceEstudioTest(TestCase):
    def test_muestra_cajas_de_texto(self):
        estudio = Estudio.objects.create(
            fecha = "01/01/1990",
            nombre = "alberto",
            ubicacion = "caracas",
            humedad = "seco",
            numero_mediciones = 2,
        )
            
        response = self.client.get('/wenner/%d/introduce_estudio' % (estudio.id))

        self.assertContains(response, '<input id="id_fecha" name="fecha_texto" placeholder="Fecha" value="%s" />' % (estudio.fecha))
        self.assertContains(response, '<input id="id_nombre" name="nombre_texto"  placeholder="Nombre" value="%s"/>' % (estudio.nombre))
        self.assertContains(response, '<input id="id_ubicacion" name="ubicacion_texto" placeholder="Ubicación" value="%s" />' % (estudio.ubicacion))
        self.assertContains(response, '<input id="id_humedad" name="humedad_texto" placeholder="Humedad" value="%s" />' % (estudio.humedad))

        self.assertContains(response, '<input id="a0" name="a" type="number" step="any" placeholder="Distancia 1" required />')
        self.assertContains(response, '<input id="rho0" name="rho" type="number" step="any" placeholder="Resistividad 1" required />')

class IntroduceModeloTest(TestCase):
    def test_muestra_cajas_espesor_y_resistividad(self):
        estudio_ = Estudio.objects.create(
            fecha = "01/01/1990",
            nombre = "alberto",
            ubicacion = "caracas",
            humedad = "seco",
            numero_mediciones = 2,
        )

        fig = Figure()
        ax=fig.add_subplot(111)
        ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
        canvas=FigureCanvas(fig)

        f = BytesIO()
        fig.savefig(f)
        content_file = ContentFile(f.getvalue())
        estudio_.grafico_r_funcion_a.save('%s.png' % (estudio_.id), content_file)

        Medicion.objects.create(distancia = "2.0", resistividad = "3.103", estudio = estudio_)
        Medicion.objects.create(distancia = "1.2", resistividad = "3.151", estudio = estudio_)

        response = self.client.post(
            '/wenner/%d/introduce_modelo' % (estudio_.id),
            {"cantidad_estratos": "2"},
        )

        self.assertContains(response, '<input id="id_fecha" name="fecha_texto" placeholder="Fecha" value="%s" />' % (estudio_.fecha))
        self.assertContains(response, '<input id="id_nombre" name="nombre_texto"  placeholder="Nombre" value="%s"/>' % (estudio_.nombre))
        self.assertContains(response, '<input id="id_ubicacion" name="ubicacion_texto" placeholder="Ubicación" value="%s" />' % (estudio_.ubicacion))
        self.assertContains(response, '<input id="id_humedad" name="humedad_texto" placeholder="Humedad" value="%s" />' % (estudio_.humedad))

        medicion = estudio_.medicion_set.all()

        self.assertContains(response, '<input id="a0" name="a" type="number" step="any" placeholder="Distancia 1" value="%.3f" required />' % (medicion[0].distancia))
        self.assertContains(response, '<input id="rho0" name="rho" type="number" step="any" placeholder="Resistividad 1" value="%.3f" required />' % (medicion[0].resistividad))
        self.assertContains(response, '<input id="a1" name="a" type="number" step="any" placeholder="Distancia 2" value="%.3f" required />' % (medicion[1].distancia))
        self.assertContains(response, '<input id="rho1" name="rho" type="number" step="any" placeholder="Resistividad 2" value="%.3f" required />' % (medicion[1].resistividad))
        self.assertContains(response, '<img id="grafico_r_funcion_a"')

        self.assertContains(response, '<input id="espesor0" name="espesor" type="number" step="any" placeholder="Espesor del estrato 1" required />')
        self.assertContains(response, '<input id="rho_m0" name="rho_m" type="number" step="any" placeholder="Resistividad del estrato 1" required />')
        self.assertContains(response, '<input id="rho_m1" name="rho_m" type="number" step="any" placeholder="Resistividad del estrato 2" required />')

class VerEstudioTest(TestCase):
    def test_muestra_mediciones_e_imagen_despues_de_segundo_submit(self):
        estudio_ = Estudio.objects.create(
            fecha = "01/01/1990",
            nombre = "alberto",
            ubicacion = "caracas",
            humedad = "seco",
            numero_mediciones = 2,
        )

        Medicion.objects.create(distancia = "2.0", resistividad = "3.103", estudio = estudio_)
        Medicion.objects.create(distancia = "1.2", resistividad = "3.151", estudio = estudio_)

        fig = Figure()
        ax=fig.add_subplot(111)
        ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
        canvas=FigureCanvas(fig)

        f = BytesIO()
        fig.savefig(f)
        content_file = ContentFile(f.getvalue())
        estudio_.grafico_r_funcion_a.save('%s.png' % (estudio_.id), content_file)

        response = self.client.get('/wenner/%d/modifica_estudio' % (estudio_.id), follow=True)

        self.assertContains(response, '<input id="id_fecha" name="fecha_texto" placeholder="Fecha" value="%s" />' % (estudio_.fecha))
        self.assertContains(response, '<input id="id_nombre" name="nombre_texto"  placeholder="Nombre" value="%s"/>' % (estudio_.nombre))
        self.assertContains(response, '<input id="id_ubicacion" name="ubicacion_texto" placeholder="Ubicación" value="%s" />' % (estudio_.ubicacion))
        self.assertContains(response, '<input id="id_humedad" name="humedad_texto" placeholder="Humedad" value="%s" />' % (estudio_.humedad))

        medicion = estudio_.medicion_set.all()

        self.assertContains(response, '<input id="a0" name="a" type="number" step="any" placeholder="Distancia 1" value="%.3f" required />' % (medicion[0].distancia))
        self.assertContains(response, '<input id="rho0" name="rho" type="number" step="any" placeholder="Resistividad 1" value="%.3f" required />' % (medicion[0].resistividad))
        self.assertContains(response, '<input id="a1" name="a" type="number" step="any" placeholder="Distancia 2" value="%.3f" required />' % (medicion[1].distancia))
        self.assertContains(response, '<input id="rho1" name="rho" type="number" step="any" placeholder="Resistividad 2" value="%.3f" required />' % (medicion[1].resistividad))
        self.assertContains(response, '<img id="grafico_r_funcion_a"')

class VerModeloTest(TestCase):
    def test_muestra_mediciones_e_imagen_despues_de_segundo_submit(self):
        estudio_ = Estudio.objects.create(
            fecha = "01/01/1990",
            nombre = "alberto",
            ubicacion = "caracas",
            humedad = "seco",
            numero_mediciones = 2,
            cantidad_estratos = 3,
        )

        Medicion.objects.create(distancia = 2.0, resistividad = 3.103, estudio = estudio_)
        Medicion.objects.create(distancia = 1.2, resistividad = 3.151, estudio = estudio_)

        Estrato.objects.create(resistividad = 1.0, espesor = 4.0, estudio = estudio_)
        Estrato.objects.create(resistividad = 2.0, espesor = 5.0, estudio = estudio_)
        Estrato.objects.create(resistividad = 3.0, estudio = estudio_)

        fig = Figure()
        ax=fig.add_subplot(111)
        ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
        canvas=FigureCanvas(fig)

        f = BytesIO()
        fig.savefig(f)
        content_file = ContentFile(f.getvalue())
        estudio_.grafico_r_funcion_a.save('%s.png' % (estudio_.id), content_file)

        response = self.client.get('/wenner/%d/modifica_modelo' % (estudio_.id), follow=True)

        self.assertContains(response, '<input id="id_fecha" name="fecha_texto" placeholder="Fecha" value="%s" />' % (estudio_.fecha))
        self.assertContains(response, '<input id="id_nombre" name="nombre_texto"  placeholder="Nombre" value="%s"/>' % (estudio_.nombre))
        self.assertContains(response, '<input id="id_ubicacion" name="ubicacion_texto" placeholder="Ubicación" value="%s" />' % (estudio_.ubicacion))
        self.assertContains(response, '<input id="id_humedad" name="humedad_texto" placeholder="Humedad" value="%s" />' % (estudio_.humedad))

        medicion = estudio_.medicion_set.all()

        self.assertContains(response, '<input id="a0" name="a" type="number" step="any" placeholder="Distancia 1" value="%.3f" required />' % (medicion[0].distancia))
        self.assertContains(response, '<input id="rho0" name="rho" type="number" step="any" placeholder="Resistividad 1" value="%.3f" required />' % (medicion[0].resistividad))
        self.assertContains(response, '<input id="a1" name="a" type="number" step="any" placeholder="Distancia 2" value="%.3f" required />' % (medicion[1].distancia))
        self.assertContains(response, '<input id="rho1" name="rho" type="number" step="any" placeholder="Resistividad 2" value="%.3f" required />' % (medicion[1].resistividad))
        self.assertContains(response, '<img id="grafico_r_funcion_a"')

        estrato = estudio_.estrato_set.all()

        self.assertContains(response, '<input id="rho_m0" name="rho_m" type="number" step="any" placeholder="Resistividad del estrato 1" value="%.3f" required />' % (estrato[0].resistividad))
        self.assertContains(response, '<input id="espesor0" name="espesor" type="number" step="any" placeholder="Espesor del estrato 1" value="%.3f" required />' % (estrato[0].espesor))
        self.assertContains(response, '<input id="rho_m1" name="rho_m" type="number" step="any" placeholder="Resistividad del estrato 2" value="%.3f" required />' % (estrato[1].resistividad))
        self.assertContains(response, '<input id="espesor1" name="espesor" type="number" step="any" placeholder="Espesor del estrato 2" value="%.3f" required />' % (estrato[1].espesor))
        self.assertContains(response, '<input id="rho_m2" name="rho_m" type="number" step="any" placeholder="Resistividad del estrato 3" value="%.3f" required />' % (estrato[2].resistividad))

class MedicionesCampoTest(TestCase):

    def test_guarda_y_recupera_mediciones(self):
        from datetime import date
        estudio = Estudio()
        estudio.save()

        primera_medicion = Medicion()
        primera_medicion.distancia = 8.31
        primera_medicion.resistividad = 52.71
        primera_medicion.estudio = estudio
        primera_medicion.save()

        segunda_medicion = Medicion()
        segunda_medicion.distancia = 4.5
        segunda_medicion.resistividad = 56.55
        segunda_medicion.estudio = estudio
        segunda_medicion.save()

        mediciones_guardadas = Medicion.objects.all()
        self.assertEqual(mediciones_guardadas.count(), 2)

        primera_medicion_guardada = mediciones_guardadas[0]
        segunda_medicion_guardada = mediciones_guardadas[1]
        self.assertEqual(primera_medicion_guardada.distancia, 8.31)
        self.assertEqual(primera_medicion_guardada.resistividad, 52.71)
        self.assertEqual(segunda_medicion_guardada.distancia, 4.5)
        self.assertEqual(segunda_medicion_guardada.resistividad, 56.55)
