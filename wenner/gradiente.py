#-*- coding: utf-8 -*-
from pprint import pformat
from math import exp
from math import log
from math import sqrt
from numpy import matrix
import numpy

def ajuste(it, pl, st, g, gr, qv, q, bn, bm, jn, jm, p):
    for i in range(1, it+1):
        pl[i] = p[i]
        p[i] = p[i] * (1+st*g[i]/gr)
    qv = q
    bn = bm
    jn = jm
    return (it, pl, st, g, gr, qv, q, bn, bm, jn, jm)

def gradiente(il, p, jz, xw, rf):
    bn = 0
    jn = 0
    (f, qv, st, ic, iss, e, imax) = (exp(log(10)/6), 99, 0, 0, 0, .001, 200)
#    xw = [0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 18, 20, 20, 16, 18, 20]
#    rf = [0, 2356.2, 1153.6, 776.6, 638.37, 496.4, 490, 301.6, 201, 184, 242.3, 301.6, 339.3, 377, 62.83, 11.06, 9.05, 5.03] # verificar que los datos sean correctos
    d = numpy.zeros(21**2).reshape((21,21))
    rmod = [0 for x in range(21)]
    g = [0 for x in range(21)]
    t = [0 for x in range(21)]
    dr = [0 for x in range(21)]
    pl = [0 for x in range(21)]
    r1 = p[2]
    r2 = p[3]
    kr = (r2-r1)/(r2+r1)
    kr1 = kr-1

    ik = il-1
    it = il+ik

    bandera = 1
    while 1:
        if bandera == 1:
            ic = ic + 1
            bandera = 0
        q = 0
        bm = 0
        for i in range(1, it+1):
            g[i] = 0
        for mi in range(1, jz+1):
            xr = xw[mi]
            j = 1
            x = xr / 341.2788892
            lb = 1
            for l in range(1, 10+1):
                b = p[it]
                bc = b
                for k in range(1, ik+1):
                    dw = p[il-k]
                    rw = p[it-k]
                    if abs(dw/x) > 7:
                        th = 1
                    else:
                        aex=exp(dw/x)
                        bex=1/aex
                        th = (aex-bex)/(aex+bex)
                    b = (b+th*rw)/(1+th*b/rw)
                t[l] = b
                x = x*f**2
            x = xr/341.2788892
            for l in range(1, 10+1):
                d[l, it] = 1
                b = bc
                for k in range(1, ik+1):
                    i = il-k
                    iw = it-k
                    iy = iw+1
                    dw = p[i]
                    rw=p[iw]
                    if abs(dw/x) > 7:
                        th = 1
                    else:
                        aex=exp(dw/x)
                        bex = 1/aex
                        th = (aex-bex)/(aex+bex)
                    ba = 1+b*th/rw
                    pa = (1-th**2)/ba**2
                    brw = b/rw
                    d[l, i] = (rw-b*brw)/x
                    d[l, iw] = th*(1+brw**2+2*th*brw)/ba**2
                    for iz in range(i, ik+1):
                        d[l, iz] = pa*d[l, iz]
                    for iz in range(iy, it+1):
                        d[l, iz] = pa*d[l, iz]
                    b = (b + th * rw)/(1 + th * brw)
                x = x * f**2
            rm = -.0067 * t[1] + .0179 * t[2] - .0253 * t[3] + .0416 * t[4] - .0935 * t[5]
            rm = rm + .3473 * t[6] - 1.3341 * t[7] + 1.5662 * t[8] + .4582 * t[9] + .0284 * t[10]
            rmod[mi] = rm
            ba = 1-rm/rf[mi]
            for i in range(1, it+1):
                b = -.0067 * d[1, i] + .0179 * d[2, i] - .0253 * d[3, i] + .0416*d[4, i] - .0935*d[5, i]
                b = b + .3473 * d[6, i] - 1.3341 * d[7, i] + 1.5662 * d[8, i] + .4582 * d[9, i] + .0284 * d[10, i]
                g[i] = g[i] + 2 * ba * b * p[i] / rf[mi]
            q = q + ba**2
            if bm**2 < ba**2:
                bm = -ba
                jm = mi

        q = sqrt(q/jz)
        if qv <= q:
            iss = 1
            for i in range(1, it+1):
                p[i] = .67 * pl[i] + .33 * p[i]
                pl[i] = p[i]
            st = st/3
            if st > .003:
                continue
            else:
                for i in range(1, it+1):
                    p[i] = pl[i]
                qv = q
                bn = bm
                jn = jm
                return (ik, it, p, q, jm, ic, jz, xw, rf, rmod, bm)
                quit()
        else:
            if e >= q or ic > imax:
                return (ik, it, p, q, jm, ic, jz, xw, rf, rmod, bm)
                quit()
            b = 0
            for i in range(1, it+1):
                g[i] = g[i] / jz
                b = b + g[i]**2
            gr = sqrt(b)
            if iss > 0:
                (it, pl, st, g, gr, qv, q, bn, bm, jn, jm) = ajuste(it, pl, st, g, gr, qv, q, bn, bm, jn, jm, p)
                bandera = 1
                continue
            st = (q**2 - .9 * e**2)/gr
            if (.5 - st) >= 0:
                (it, pl, st, g, gr, qv, q, bn, bm, jn, jm) = ajuste(it, pl, st, g, gr, qv, q, bn, bm, jn, jm, p)
                bandera = 1
                continue
            st = .5
            (it, pl, st, g, gr, qv, q, bn, bm, jn, jm) = ajuste(it, pl, st, g, gr, qv, q, bn, bm, jn, jm, p)
            bandera = 1
            continue
