"""sgi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'wenner.views.home_page', name='home'),
    url(r'^wenner/$', 'wenner.views.home_page', name='home'),

    url(r'^wenner/crea_estudio$', 'wenner.views.crea_estudio', name='crea_estudio'),
    url(r'^wenner/(\d+)/introduce_estudio$', 'wenner.views.introduce_estudio', name='introduce_estudio'),
    url(r'^wenner/(\d+)/modifica_estudio$', 'wenner.views.modifica_estudio', name='modifica_estudio'),
    url(r'^wenner/(\d+)/ver_estudio$', 'wenner.views.ver_estudio', name='ver_estudio'),

    url(r'^wenner/(\d+)/modifica_modelo$', 'wenner.views.modifica_modelo', name='modifica_modelo'),
    url(r'^wenner/(\d+)/introduce_modelo$', 'wenner.views.introduce_modelo', name='introduce_modelo'),
    url(r'^wenner/(\d+)/ver_modelo$', 'wenner.views.ver_modelo', name='ver_modelo'),

    url(r'^wenner/graficos/(.+).png$', 'wenner.views.grafico_rho_funcion_a', name='grafico_rho_funcion_a'),
    url(r'^wenner/(\d+)/reporte_pdf$', 'wenner.views.reporte_pdf', name='crea_reporte_pdf'),
]
