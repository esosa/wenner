from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.files.base import ContentFile
from django.core.files.base import File
from wenner.models import Estudio, Medicion, Estrato

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

import datetime
import reportlab

from io import BytesIO
from wenner.util import crea_modelo_estratos

def home_page(request):
    return render(request, 'home.html', {
        "range": range(30)
    })

def crea_estudio(request):
    num_mediciones = int(request.POST.get('num_mediciones', 0))
    fecha_texto = request.POST.get('fecha_texto', '')
    nombre_texto = request.POST.get('nombre_texto', '')
    ubicacion_texto = request.POST.get('ubicacion_texto', '')
    humedad_texto = request.POST.get('humedad_texto', '')

    estudio = Estudio.objects.create(
        fecha=fecha_texto,
        nombre=nombre_texto,
        ubicacion=ubicacion_texto,
        humedad=humedad_texto,
        numero_mediciones=num_mediciones,
    )
    return redirect('/wenner/%d/introduce_estudio' % (estudio.id,))

def introduce_estudio(request, estudio_id):
    estudio = Estudio.objects.get(id = estudio_id)
    request.session["desde"] = "introduce_estudio"

    return render(request, 'introduce_estudio.html', {
        'range': range(len(estudio.medicion_set.all()), estudio.numero_mediciones),
        'estudio': estudio,
    })

def modifica_estudio(request, estudio_id):
    estudio_ = Estudio.objects.get(id = estudio_id)

    fecha_texto = request.POST.get('fecha_texto', '')
    nombre_texto = request.POST.get('nombre_texto', '')
    ubicacion_texto = request.POST.get('ubicacion_texto', '')
    humedad_texto = request.POST.get('humedad_texto', '')
    numero_mediciones = int(request.POST.get('num_mediciones', 0))

    a = request.POST.getlist('a')
    rho = request.POST.getlist('rho')
    medicion_id = request.POST.getlist('medicion_id', [])

    if fecha_texto != '' and fecha_texto != estudio_.fecha:
        estudio_.fecha = fecha_texto
    if nombre_texto != '' and nombre_texto != estudio_.nombre:
        estudio_.nombre = nombre_texto
    if ubicacion_texto != '' and ubicacion_texto != estudio_.ubicacion:
        estudio_.ubicacion = ubicacion_texto
    if humedad_texto != '' and humedad_texto != estudio_.humedad:
        estudio_.humedad = humedad_texto

    if medicion_id == []:
        for x, y in zip(a, rho):
            if not estudio_.medicion_set.all().filter(distancia=x, resistividad=y).exists():
                medicion = Medicion.objects.create(distancia=x, resistividad=y, estudio=estudio_)

                fig = Figure()
                ax=fig.add_subplot(111)
                ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
                canvas=FigureCanvas(fig)

                f = BytesIO()
                fig.savefig(f)
                content_file = ContentFile(f.getvalue())
                estudio_.grafico_r_funcion_a.save('grfa%s.png' % (estudio_.id), content_file)
    else:
        while len(a) > len(medicion_id):
            medicion_id.append(None)
        for x, y, z in zip(a, rho, medicion_id):
            if z != None:
                medicion = Medicion.objects.get(id=z)
                medicion.distancia = x
                medicion.resistividad = y
                medicion.save()
            else:
                Medicion.objects.create(distancia=x, resistividad=y, estudio=estudio_)

            fig = Figure()
            ax=fig.add_subplot(111)
            ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
            canvas=FigureCanvas(fig)

            f = BytesIO()
            fig.savefig(f)
            content_file = ContentFile(f.getvalue())
            estudio_.grafico_r_funcion_a.save('grfa%s.png' % (estudio_.id), content_file)

    if numero_mediciones != 0 and numero_mediciones != estudio_.numero_mediciones:
        if estudio_.numero_mediciones > numero_mediciones:
            for i in range(estudio_.numero_mediciones-numero_mediciones):
                estudio_.medicion_set.all().last().delete()
                if request.session["desde"] == "ver_estudio":
                    fig = Figure()
                    ax=fig.add_subplot(111)
                    ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()])
                    canvas=FigureCanvas(fig)

                    f = BytesIO()
                    fig.savefig(f)
                    content_file = ContentFile(f.getvalue())
                    estudio_.grafico_r_funcion_a.save('grfa%s.png' % (estudio_.id), content_file)
        estudio_.numero_mediciones = numero_mediciones
        estudio_.save()
        if request.session["desde"] == "introduce_estudio":
            return redirect('/wenner/%d/introduce_estudio' % (estudio_.id,))
    else:
        estudio_.save()
        if request.session["desde"] == "introduce_estudio":
            return redirect('/wenner/%d/ver_estudio#grafico_r_funcion_a' % (estudio_.id,))
    estudio_.save()
    return redirect('/wenner/%d/ver_estudio#grafico_r_funcion_a' % (estudio_.id,))

def ver_estudio(request, estudio_id):
    request.session["desde"] = "ver_estudio"
    estudio = Estudio.objects.get(id = estudio_id)

    return render(request, 'ver_estudio.html', {
        'estudio': estudio,
        'range': range(len(estudio.medicion_set.all()), estudio.numero_mediciones),
    })

def introduce_modelo(request, estudio_id):
    estudio = Estudio.objects.get(id = estudio_id)
    if request.session["desde"] == "ver_estudio":
        estudio.cantidad_estratos = int(request.POST.get('cantidad_estratos', 0))
        estudio.save()

    request.session["desde"] = "introduce_modelo"
    return render(request, 'introduce_modelo.html', {
        'estudio': estudio,
        'range': range(len(estudio.estrato_set.all()), estudio.cantidad_estratos),
        'lim': len(estudio.estrato_set.all())-1
    })

def modifica_modelo(request, estudio_id):
    from wenner.gradiente import gradiente
    estudio_ = Estudio.objects.get(id = estudio_id)
    espesor = request.POST.getlist('espesor') + [-1]
    rho_m = request.POST.getlist('rho_m')
    estrato_id = request.POST.getlist('estrato_id', [])
    cantidad_estratos = int(request.POST.get('cantidad_estratos', 0))

    print(estudio_.estrato_set.all())
    if request.session["desde"] == "introduce_modelo":
        if estrato_id == []:
            print("vacio")
            print(estudio_.cantidad_estratos)
            # crea objetos
            for x, y in zip(rho_m, espesor):
                Estrato.objects.create(resistividad = x, espesor = y, estudio = estudio_)
        else:
            # modificamos objetos existentes
            for x, y, id_ in zip(rho_m, espesor, estrato_id):
                estrato = Estrato.object.get(id = id_)
                estrato.resistividad = x
                estrato.espesor = y
                estrato.save()

        print(estudio_.cantidad_estratos)
        print(type(estudio_.cantidad_estratos))
        print(cantidad_estratos)
        print(type(cantidad_estratos))
        if cantidad_estratos != estudio_.cantidad_estratos:
            if cantidad_estratos < estudio_.cantidad_estratos:
                for estrato in range(estudio_.cantidad_estratos - cantidad_estratos):
                # borramos objetos sobrantes
                    estrato = estudio_.estrato_set.all().last()
                    estrato.delete()
            if cantidad_estratos > estudio_.cantidad_estratos:
                # agregamos objetos
                pass
            estudio_.cantidad_estratos = cantidad_estratos
            estudio_.save()
            return redirect('/wenner/%d/introduce_modelo' % (estudio_.id,))
        else:
            crea_modelo_estratos(request, estudio_)
            return redirect('/wenner/%d/ver_modelo' % (estudio_.id,))
                
    else:
        pass
        # bla bla
                

def modifica_modelo_original(request, estudio_id):
    from wenner.gradiente import gradiente
    estudio_ = Estudio.objects.get(id = estudio_id)

    espesor = request.POST.getlist('espesor') + [-1]
    rho_m = request.POST.getlist('rho_m')
    estrato_id = request.POST.getlist('estrato_id', [])
    cantidad_estratos = int(request.POST.get('cantidad_estratos', 0))

    if estrato_id == []:
        for x, y in zip(espesor, rho_m):
            if not estudio_.estrato_set.all().filter(espesor=x, resistividad=y).exists():
                espesor = Estrato.objects.create(espesor=x, resistividad=y, estudio=estudio_)
    else:
        while len(rho_m) > len(estrato_id):
            estrato_id.append(None)
        for x, y, z in zip(espesor, rho_m, estrato_id):
            if z != None:
                estrato = Estrato.objects.get(id=z)
                estrato.espesor = x
                estrato.resistividad = y
                estrato.save()
            else:
                Estrato.objects.create(espesor=x, resistividad=y, estudio=estudio_)

    p = [0] + [x.espesor for x in estudio_.estrato_set.all()][:-1] + [x.resistividad for x in estudio_.estrato_set.all()]
    p += [0 for x in range(21-len(p))]
    xw = [0] + [x.distancia for x in estudio_.medicion_set.all()]
    rf = [0] + [x.resistividad for x in estudio_.medicion_set.all()]
    il = estudio_.cantidad_estratos
    jz = estudio_.numero_mediciones

    (ik, it, p, q, jm, ic, jz, xw, rf, rmod, bm) = gradiente(il, p, jz, xw, rf)
    p = p[1:]
    p1 = []
    p2 = []
    for i in range(ik):
        p1.append(p[i+ik])
        p2.append(p[i])
    p0 = zip(p1, p2)
    p3 = p[it-1]
    rf = rf[1:]
    rmod = rmod[1:]
    xw = xw[1:]

    rf_rmod = []
    for i in range(jz):
        rf_rmod.append(1-rmod[i]/rf[i])

    fig = Figure()
    ax=fig.add_subplot(111)
    ax.plot(xw[:jz], rmod[:jz], color="red", marker="*",label="Resistividad Campo")
    ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()], color="blue", marker="o",label="Resistividad Modelo")

    canvas=FigureCanvas(fig)

    f = BytesIO()
    fig.savefig(f)
    content_file = ContentFile(f.getvalue())
    estudio_.grafico_modelo.save('gm%s.png' % (estudio_.id), content_file)
    estudio_.save()


    if cantidad_estratos != 0 and cantidad_estratos != estudio_.cantidad_estratos:
        if estudio_.cantidad_estratos > cantidad_estratos:
            for i in range(estudio_.cantidad_estratos-cantidad_estratos):
                estudio_.estrato_set.all().last().delete()
            un_estrato = estudio_.estrato_set.all().last()
            un_estrato.espesor = -1.0
            un_estrato.save()
            estudio_.cantidad_estratos = cantidad_estratos
            estudio_.save()
            if request.session["desde"] == "ver_modelo":
                p = [0] + [x.espesor for x in estudio_.estrato_set.all()][:-1] + [x.resistividad for x in estudio_.estrato_set.all()]
                p += [0 for x in range(21-len(p))]
                xw = [0] + [x.distancia for x in estudio_.medicion_set.all()]
                rf = [0] + [x.resistividad for x in estudio_.medicion_set.all()]
                il = estudio_.cantidad_estratos
                jz = estudio_.numero_mediciones

                (ik, it, p, q, jm, ic, jz, xw, rf, rmod, bm) = gradiente(il, p, jz, xw, rf)
                p = p[1:]
                p1 = []
                p2 = []
                for i in range(ik):
                    p1.append(p[i+ik])
                    p2.append(p[i])
                p0 = zip(p1, p2)
                p3 = p[it-1]
                rf = rf[1:]
                rmod = rmod[1:]
                xw = xw[1:]

                rf_rmod = []
                for i in range(jz):
                    rf_rmod.append(1-rmod[i]/rf[i])

                fig = Figure()
                ax=fig.add_subplot(111)
                ax.plot(xw[:jz], rmod[:jz], color="red", marker="*",label="Resistividad Campo")
                ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()], color="blue", marker="o",label="Resistividad Modelo")

                canvas=FigureCanvas(fig)

                f = BytesIO()
                fig.savefig(f)
                content_file = ContentFile(f.getvalue())
                estudio_.grafico_modelo.save('gm%s.png' % (estudio_.id), content_file)
                estudio_.save()
                (request.session["ik"], request.session["it"], request.session["p1"], request.session["p2"], request.session["p3"], request.session["q"], request.session["jm"], request.session["ic"], request.session["jz"], request.session["xw"], request.session["rf"], request.session["rmod"], request.session["bm"])  = (ik, it, p1, p2, p3, q, jm, ic, jz, xw, rf, rmod, bm)

        if request.session["desde"] == "introduce_modelo":
            return redirect('/wenner/%d/introduce_modelo' % (estudio_.id,))
    else:
        estudio_.save()
        if request.session["desde"] == "introduce_estudio":
            return redirect('/wenner/%d/ver_modelo' % (estudio_.id,))
    estudio_.save()
    return redirect('/wenner/%d/ver_modelo' % (estudio_.id,))

def ver_modelo(request, estudio_id):
    estudio_ = Estudio.objects.get(id = estudio_id)
    request.session["desde"] = "ver_modelo"

    (ik, it, p1, p2, p3, q, jm, ic, jz, xw, rf, rmod, bm) = (request.session["ik"], request.session["it"], request.session["p1"], request.session["p2"], request.session["p3"], request.session["q"], request.session["jm"], request.session["ic"], request.session["jz"], request.session["xw"], request.session["rf"], request.session["rmod"], request.session["bm"])

    p0 = zip(p1, p2)
    rf_rmod = []
    for i in range(jz):
        rf_rmod.append(1-rmod[i]/rf[i])

    return render(request, 'ver_modelo.html', {
        'estudio': estudio_,
        'ik': ik,
        'it': it,
        'p0': p0,
        'p3': p3,
        'q': q,
        'jm': jm,
        'ic': ic,
        'jz': jz,
        'r' : zip(xw, rf, rmod, rf_rmod),
        'bm': bm,
    })


def reporte_pdf(request, estudio_id):
    from reportlab.platypus import SimpleDocTemplate, Paragraph
    from reportlab.lib.styles import ParagraphStyle
    from reportlab.lib import colors
    from reportlab.platypus.tables import Table, TableStyle
    from reportlab.lib.pagesizes import A4, cm
    from reportlab.platypus.figures import ImageFigure
    from reportlab.platypus.flowables import Image
    from reportlab.platypus.frames import Frame
    import os

    estudio = Estudio.objects.get(id = estudio_id)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'inline; filename="reporte.pdf"'

    doc = SimpleDocTemplate(response, rightMargin=2.5*cm, leftMargin=2.5 * cm, topMargin=2 * cm, bottomMargin=0)

    elements = []

    frame = Frame(10, 10, 100, 40, showBoundary=1)
    data = [['Distancia', 'Resistividad']]
    data += [[x.distancia, x.resistividad] for x in estudio.medicion_set.all()]
    tabla = Table(data, colWidths=80, rowHeights=20)
    tabla.setStyle(TableStyle([
        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
        ('BOX', (0,0), (-1,-1), 0.25, colors.black),
    ]))
    frame.add(tabla, doc)

    ruta_grafico_r_funcion_a = os.path.join(os.getcwd(), estudio.grafico_r_funcion_a.name)
    grafico_r_funcion_a = ImageFigure(ruta_grafico_r_funcion_a, 'la resistividad en función de la distancia', hAlign = 'CENTER', border = True)

    estilo  = ParagraphStyle(name='fancy')
    texto="aa"
    parrafo = Paragraph(texto, estilo)

    elements.append(tabla)
    elements.append(grafico_r_funcion_a)
    elements.append(parrafo)

    doc.build(elements) 

    return response

def grafico_rho_funcion_a(request, estudio_id):
    f = open("wenner/graficos/%s.png" % estudio_id, mode="rb")
    imagen = f.read()
    response = HttpResponse(imagen, content_type='image/png')
    return response

def grafico_modelo(request, estudio_id):
    f = open("wenner/graficos/%s.png" % estudio_id, mode="rb")
    imagen = f.read()
    response = HttpResponse(imagen, content_type='image/png')
    return response
