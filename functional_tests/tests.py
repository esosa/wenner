from django.test import LiveServerTestCase
from django.conf import settings
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from time import sleep
from re import search
import unittest

class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def revisamos_palabras_en_texto(self, palabras, texto):
        for palabra in palabras:
            self.assertIn(palabra, texto)

    def revisamos_placeholders_en_entradas(self, caja_entrada, placeholder):
        self.assertEqual(
            caja_entrada.get_attribute('placeholder'),
            placeholder
        )

    def test_recibe_resistividades_y_grafica(self):
        # alberto visita la página
        self.browser.get(self.live_server_url)
        # alberto ve el título de la página y la cabecera
        self.assertIn("Resistividad", self.browser.title)
        texto_cabecera = self.browser.find_element_by_tag_name("h1").text
        self.assertIn("modelo", texto_cabecera)
        # lee un mensaje de bienvenida que le explica a alberto que hace el programa y que parámetros se necesitan inicialmente (resistividades aparentes, distancias), aparte de, opcionalmente, el nombre de la persona usando la aplicación, la ubicación en donde se realizaron estas medidas, y la humedad del terreno al tomar las mediciones
        texto_bienvenida = self.browser.find_element_by_id("texto_bienvenida").text
        self.revisamos_palabras_en_texto(('fecha', 'nombre', 'ubicación', 'humedad', 'resistividad'), texto_bienvenida)
           
        # también alberto, aparte de el mensaje de bienvenida, puede ver los cuadros de texto que le pide estos datos, y una lista que le pide el número de mediciones que se tomarán en consideración
        # alberto introduce los datos del estudio que hizo
        entrada_fecha = self.browser.find_element_by_id('id_fecha')
        entrada_nombre = self.browser.find_element_by_id('id_nombre')
        entrada_ubicacion = self.browser.find_element_by_id('id_ubicacion')
        entrada_humedad = self.browser.find_element_by_id('id_humedad')
        entrada_num_mediciones = self.browser.find_element_by_id('id_num_mediciones')

        self.revisamos_placeholders_en_entradas(entrada_fecha, 'Fecha')
        self.revisamos_placeholders_en_entradas(entrada_nombre, 'Nombre')
        self.revisamos_placeholders_en_entradas(entrada_ubicacion, 'Ubicación')
        self.revisamos_placeholders_en_entradas(entrada_humedad, 'Humedad del terreno')
        self.revisamos_placeholders_en_entradas(entrada_num_mediciones, 'Número de mediciones')

        fecha = '03/09/2015'
        nombre = 'Lic. Alberto Tarufeti'
        ubicacion = 'Calle Roberti'
        humedad = 'Medianamente húmedo'
        num_mediciones = '3'

        entrada_fecha.send_keys(fecha)
        entrada_nombre.send_keys(nombre)
        entrada_ubicacion.send_keys(ubicacion)
        entrada_humedad.send_keys(humedad)
        entrada_num_mediciones.send_keys(num_mediciones)

        enviar = self.browser.find_element_by_id('enviar')
        enviar.click()


        # cuando alberto pone que quiere considerar 3 medidas y presiona el botón para enviar el formulario, aparece en la página, aparte de lo que había antes, casillas para introducir las distancias y las resistividades
        # esta página tiene un url asociado a los datos que está introduciendo
        # alberto introduce los datos de su estudio en las casillas que indican que datos ingresar

        url_estudio = self.browser.current_url
        self.assertRegex(url_estudio, '/wenner/\d+/introduce_estudio')


        entrada_fecha = self.browser.find_element_by_id('id_fecha')
        entrada_nombre = self.browser.find_element_by_id('id_nombre')
        entrada_ubicacion = self.browser.find_element_by_id('id_ubicacion')
        entrada_humedad = self.browser.find_element_by_id('id_humedad')

        self.assertEqual(entrada_fecha.get_attribute("value"), fecha)
        self.assertEqual(entrada_nombre.get_attribute("value"), nombre)
        self.assertEqual(entrada_ubicacion.get_attribute("value"), ubicacion)
        self.assertEqual(entrada_humedad.get_attribute("value"), humedad)

        a = ('1', '2', '3')
        rho = ('52.71', '56.55', '64.09')
        for i in range(3):
            entrada_a = self.browser.find_element_by_id('a%d' % i)
            self.revisamos_placeholders_en_entradas(entrada_a, 'Distancia %d' % (i+1))
            entrada_a.send_keys(a[i])

            entrada_rho = self.browser.find_element_by_id('rho%d' % i)
            self.revisamos_placeholders_en_entradas(entrada_rho, 'Resistividad %d' % (i+1))
            entrada_rho.send_keys(rho[i])
    
        enviar = self.browser.find_element_by_id('enviar')
        enviar.click()
        # al haber alberto introducido las mediciones y haber presionado entre o un botón que dice "continuar", aparte de poder todo lo que introdujo antes, puede ver un gráfico que muestra las resistividades en funcion de las distancias, y se le indica que indique el número de estratos de su modelo

        url_estudio = self.browser.current_url
        self.assertRegex(url_estudio, '/wenner/\d+')

        entrada_fecha = self.browser.find_element_by_id('id_fecha')
        entrada_nombre = self.browser.find_element_by_id('id_nombre')
        entrada_ubicacion = self.browser.find_element_by_id('id_ubicacion')
        entrada_humedad = self.browser.find_element_by_id('id_humedad')

        self.assertEqual(entrada_fecha.get_attribute("value"), fecha)
        self.assertEqual(entrada_nombre.get_attribute("value"), nombre)
        self.assertEqual(entrada_ubicacion.get_attribute("value"), ubicacion)
        self.assertEqual(entrada_humedad.get_attribute("value"), humedad)

        for i in range(3):
            entrada_a = self.browser.find_element_by_id('a%d' % i)
            self.assertEqual(entrada_a.get_attribute("value"), "%.3f" % (float(a[i])))
            entrada_rho = self.browser.find_element_by_id('rho%d' % i)
            self.assertEqual(entrada_rho.get_attribute("value"), "%.3f" % (float(rho[i])))

        grafico_r_funcion_a = self.browser.find_element_by_id("grafico_r_funcion_a")
        self.assertEqual(grafico_r_funcion_a.is_displayed(), True)

        texto_instructivo_2 = self.browser.find_element_by_id("texto_instructivo_2").text
        self.revisamos_palabras_en_texto(('modelo', 'estrat', 'resistividad', 'espesor'), texto_instructivo_2)
        
        entrada_cantidad_estratos = self.browser.find_element_by_id('id_cantidad_estratos')
        entrada_cantidad_estratos.send_keys('3')
        segundo_enviar = self.browser.find_element_by_id('segundo_enviar')
        segundo_enviar.click()

        # alberto puede ver el formulario que llenó antes, con los datos que introdujo antes, y el gráfico asociado a su estudio. aparte, se le indica que puede introducir los datos del modelo y puede ver las casillas para introducir los datos de espesor y resistividad de su modelo

        entrada_fecha = self.browser.find_element_by_id('id_fecha')
        entrada_nombre = self.browser.find_element_by_id('id_nombre')
        entrada_ubicacion = self.browser.find_element_by_id('id_ubicacion')
        entrada_humedad = self.browser.find_element_by_id('id_humedad')

        self.assertEqual(entrada_fecha.get_attribute("value"), fecha)
        self.assertEqual(entrada_nombre.get_attribute("value"), nombre)
        self.assertEqual(entrada_ubicacion.get_attribute("value"), ubicacion)
        self.assertEqual(entrada_humedad.get_attribute("value"), humedad)

        for i in range(3):
            entrada_a = self.browser.find_element_by_id('a%d' % i)
            self.assertEqual(entrada_a.get_attribute("value"), "%.3f" % (float(a[i])))
            entrada_rho = self.browser.find_element_by_id('rho%d' % i)
            self.assertEqual(entrada_rho.get_attribute("value"), "%.3f" % (float(rho[i])))

        grafico_r_funcion_a = self.browser.find_element_by_id("grafico_r_funcion_a")
        self.assertEqual(grafico_r_funcion_a.is_displayed(), True)

        texto_instructivo_2 = self.browser.find_element_by_id("texto_instructivo_2").text
        self.revisamos_palabras_en_texto(('Introduzca', 'resistividad', 'espesor'), texto_instructivo_2)

        rho_m = ('1', '2', '3')
        espesor = ('4', '5')

        for i in range(3):
            entrada_rho_m = self.browser.find_element_by_id('rho_m%d' % i)
            self.revisamos_placeholders_en_entradas(entrada_rho_m, 'Resistividad del estrato %d' % (i+1))
            entrada_rho_m.send_keys(rho_m[i])

        for i in range(2):
            entrada_espesor = self.browser.find_element_by_id('espesor%d' % i)
            self.revisamos_placeholders_en_entradas(entrada_espesor, 'Espesor del estrato %d' % (i+1))
            entrada_espesor.send_keys(espesor[i])

        segundo_enviar = self.browser.find_element_by_id('segundo_enviar')
        segundo_enviar.click()

        # alberto puede ver lo mismo que veía antes, pero ahora puede ver la tabla asociada a su modelo, además de el gráfico de antes superpuesto con las resistividades que devuelve el modelo

        entrada_fecha = self.browser.find_element_by_id('id_fecha')
        entrada_nombre = self.browser.find_element_by_id('id_nombre')
        entrada_ubicacion = self.browser.find_element_by_id('id_ubicacion')
        entrada_humedad = self.browser.find_element_by_id('id_humedad')

        self.assertEqual(entrada_fecha.get_attribute("value"), fecha)
        self.assertEqual(entrada_nombre.get_attribute("value"), nombre)
        self.assertEqual(entrada_ubicacion.get_attribute("value"), ubicacion)
        self.assertEqual(entrada_humedad.get_attribute("value"), humedad)

        for i in range(3):
            entrada_a = self.browser.find_element_by_id('a%d' % i)
            self.assertEqual(entrada_a.get_attribute("value"), "%.3f" % (float(a[i])))
            entrada_rho = self.browser.find_element_by_id('rho%d' % i)
            self.assertEqual(entrada_rho.get_attribute("value"), "%.3f" % (float(rho[i])))

        grafico_r_funcion_a = self.browser.find_element_by_id("grafico_r_funcion_a")
        self.assertEqual(grafico_r_funcion_a.is_displayed(), True)

        for i in range(3):
            entrada_rho_m = self.browser.find_element_by_id('rho_m%d' % i)
            self.assertEqual(entrada_rho_m.get_attribute("value"), "%.3f" % (float(rho_m[i])))

        for i in range(2):
            entrada_grosor = self.browser.find_element_by_id('espesor%d' % i)
            self.assertEqual(entrada_grosor.get_attribute("value"), "%.3f" % (float(espesor[i])))

        datos_modelo = {
            "resistividad_espesor": {
                'th': ["Resistividad", "Espesor"], 
                "tr": [
                    ["57.188", "11.999"],
                    ["2.331", "4.950"],
                    ["3.045", ""],
                ],
            },
            "error_rms": "0.082",
            "error_maximo": "-0.116",
            "cantidad_iteraciones": "199",
            "muestra": "3",
            "a_rc_rm_er": {
                'th': ["Distancia", "Resistividad Campo", "Resistividad Modelo", "Error Relativo"],
                "tr": [
                    ["1.000", "52.710", "56.947", "-0.080"],
                    ["2.000", "56.550", "56.892", "-0.006"],
                    ["3.000", "64.090", "56.633", "0.116"],
                ],
            },
        }

        self.assertEqual(datos_modelo["resistividad_espesor"]["th"], [x.text for x in self.browser.find_elements_by_xpath('.//table[@id="tabla_resistividad_espesor"]//th')])

        tds = []
        for td in datos_modelo["resistividad_espesor"]["tr"]:
            tds += td
        self.assertEqual(tds, [x.text for x in self.browser.find_elements_by_xpath('.//table[@id="tabla_resistividad_espesor"]//td')])

        self.assertEqual(datos_modelo["error_rms"], search('([-\d.]+)$', self.browser.find_element_by_xpath('.//p[@id="error_rms"]').text).group(1))
        self.assertEqual(datos_modelo["error_maximo"], search(': ([-\d.]+),', self.browser.find_element_by_xpath('.//p[@id="error_maximo"]').text).group(1))
        self.assertEqual(datos_modelo["muestra"], search('(\d+)$', self.browser.find_element_by_xpath('.//p[@id="error_maximo"]').text).group(1))
        self.assertEqual(datos_modelo["cantidad_iteraciones"], search('([-\d.]+)$', self.browser.find_element_by_xpath('.//p[@id="cantidad_iteraciones"]').text).group(1))

        self.assertEqual(datos_modelo["a_rc_rm_er"]["th"], [x.text for x in self.browser.find_elements_by_xpath('.//table[@id="tabla_a_rc_rm_er"]//th')])

        tds = []
        for td in datos_modelo["a_rc_rm_er"]["tr"]:
            tds += td
        self.assertEqual(tds, [x.text for x in self.browser.find_elements_by_xpath('.//table[@id="tabla_a_rc_rm_er"]//td')])

        grafico_modelo = self.browser.find_element_by_id("grafico_modelo")
        self.assertEqual(grafico_modelo.is_displayed(), True)
