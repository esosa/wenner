from django.core.files.base import ContentFile
from io import BytesIO
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from wenner.gradiente import gradiente
import numpy as np
    
def crea_modelo_estratos(request, estudio_):
    p = [0] + [x.espesor for x in estudio_.estrato_set.all()][:-1] + [x.resistividad for x in estudio_.estrato_set.all()]
    p += [0 for x in range(21-len(p))]
    xw = [0] + [x.distancia for x in estudio_.medicion_set.all()]
    rf = [0] + [x.resistividad for x in estudio_.medicion_set.all()]
    il = estudio_.cantidad_estratos
    jz = estudio_.numero_mediciones

    (ik, it, p, q, jm, ic, jz, xw, rf, rmod, bm) = gradiente(il, p, jz, xw, rf)

    p = p[1:]
    p1 = []
    p2 = []
    for i in range(ik):
        p1.append(p[i+ik])
        p2.append(p[i])
    p0 = zip(p1, p2)
    p3 = p[it-1]
    rf = rf[1:]
    rmod = rmod[1:]
    xw = xw[1:]

    rf_rmod = []
    for i in range(jz):
        rf_rmod.append(1-rmod[i]/rf[i])

    fig = Figure()
    ax=fig.add_subplot(111)
    ax.set_title('Gráfico de resistividad aparente: $\\rho$a($\\Omega$.m) vs a(m)', fontsize=12)
    rc = ax.plot(xw[:jz], rmod[:jz], color="red", marker="*",label="Resistividad Campo")
    rm = ax.plot([x.distancia for x in estudio_.medicion_set.all()], [x.resistividad for x in estudio_.medicion_set.all()], color="blue", marker="o",label="Resistividad Modelo")
    ax.legend(loc=0)
    txt = ax.text(fig.gca().get_xlim()[1]+0.1, fig.gca().get_ylim()[0], 'Fecha: %s\nUbicacion: %s\nNombre: %s\nHumedad: %s' % (estudio_.fecha, estudio_.ubicacion, estudio_.nombre, estudio_.humedad))

    canvas=FigureCanvas(fig)

    f = BytesIO()
    fig.savefig(f, bbox_extra_artists=(txt,), bbox_inches='tight')
    content_file = ContentFile(f.getvalue())
    estudio_.grafico_modelo.save('gm%s.png' % (estudio_.id), content_file)
    estudio_.save()
    (request.session["ik"], request.session["it"], request.session["p1"], request.session["p2"], request.session["p3"], request.session["q"], request.session["jm"], request.session["ic"], request.session["jz"], request.session["xw"], request.session["rf"], request.session["rmod"], request.session["bm"])  = (ik, it, p1, p2, p3, q, jm, ic, jz, xw, rf, rmod, bm)
